from minds.utils import add_url_kwargs


def test_add_url_kwargs():
    url = 'https://minds.com'
    assert add_url_kwargs(url, foo='one', bar='two') == 'https://minds.com?foo=one&bar=two'
    # quoting
    assert add_url_kwargs(url, foo='with space') == 'https://minds.com?foo=with%20space'
    assert add_url_kwargs(url, **{'with space': 'with space'}) == 'https://minds.com?with%20space=with%20space'
    # do not replace existing
    assert add_url_kwargs(url + '?foo=one', foo='one') == 'https://minds.com?foo=one'
    # do add to existing
    assert add_url_kwargs(url + '?foo=one', bar='two') == 'https://minds.com?foo=one&bar=two'
